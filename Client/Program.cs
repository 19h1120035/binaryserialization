﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Title = "Client";
            while (true)
            {
                Console.WriteLine("Press enter to send ...");
                Console.ReadLine();
                Student student = new Student
                {
                    Id = 1,
                    FirstName = "Đào Văn",
                    LastName = "Thương",
                    DateOfBirth = new DateTime(2001, 04, 20)
                };
                TcpClient client = new TcpClient();
                client.Connect(IPAddress.Loopback, 1308);

                NetworkStream stream = client.GetStream();
                BinaryWriter bw = new BinaryWriter(stream);
                byte[] data = BinarySerializer.Serialize(student);
                bw.Write(data.Length);
                stream.Write(data, 0, data.Length);
                stream.Flush();
                client.Close();
            }
        }
    }
}
