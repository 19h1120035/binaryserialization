﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class BinarySerializer
    {
        // hai phương thức sau đây sử dụng BitConverter
        // BitConverter giúp chuyển đổi 1 biến (thuộc kiểu cơ sở, trừ kiểu string) về mảng byte
        public static byte[] Serialize(Student student)
        {
            // danh sách này sẽ chứa các byte thu từng trường dữ liệu
            List<byte> data = new List<byte>();
            // Chuyển id thành mảng byte và copy vào data
            data.AddRange(BitConverter.GetBytes(student.Id));
            // đếm số byte của FirstName, chuyển thành mảng byte và copy vào data
            data.AddRange(BitConverter.GetBytes(Encoding.UTF8.GetByteCount(student.FirstName)));
            // chuyển FirstName thành mảng byte vào copy vào data
            data.AddRange(Encoding.UTF8.GetBytes(student.FirstName));
            data.AddRange(BitConverter.GetBytes(Encoding.UTF8.GetByteCount(student.LastName)));
            data.AddRange(Encoding.UTF8.GetBytes(student.LastName));
            // chuyển DateOfBirth thành long (số Ticks), chuyển thành mảng byte và copy vào data
            data.AddRange(BitConverter.GetBytes(student.DateOfBirth.Ticks));
            // Chuyển đổi list thành array
            return data.ToArray();
        }
        public static Student Deserialize(byte[] data)
        {
            Student student = new Student();
            int offset = 0;
            student.Id = BitConverter.ToInt32(data, offset);
            offset += 4;
            int length1 = BitConverter.ToInt32(data, offset);
            offset += 4;
            student.FirstName = Encoding.UTF8.GetString(data, offset, length1);
            offset += length1;
            int length2 = BitConverter.ToInt32(data, offset);
            offset += 4;
            student.LastName = Encoding.UTF8.GetString(data, offset, length2);
            offset += length2;
            student.DateOfBirth = new DateTime(BitConverter.ToInt64(data, offset));
            return student;
        }
    }
}
