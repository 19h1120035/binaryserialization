﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.IO;
using System.Net;
using System.Net.Sockets;   

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Title = "Server";
            TcpListener listener = new TcpListener(IPAddress.Any, 1308);
            listener.Start(10);
            while (true)
            {
                TcpClient client = listener.AcceptTcpClient();
                NetworkStream stream = client.GetStream();
                BinaryReader br = new BinaryReader(stream);

                int length = br.ReadInt32();
                byte[] data = br.ReadBytes(length);
                Student student = BinarySerializer.Deserialize(data);
                client.Close();

                Console.WriteLine("Raw byte array: ");
                foreach (var b in data)
                {
                    Console.Write($"{b} ");
                }
                Console.WriteLine("\r\nDeserialize object: ");
                Console.WriteLine($"ID: {student.Id}\r\nFirstName: {student.FirstName}\r\nLastName: {student.LastName}\r\nDateOfBirth: {student.DateOfBirth.ToString("dd/MM/yyyy")}");
            }

        }
    }
}
